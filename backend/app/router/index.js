import Router from 'koa-router';

import NumbersController from '../controllers/numbers-controller.js';

const router = new Router();

router.get('/api/numbers/:type', NumbersController.getNumber);

router.get('/', async (ctx) => {
  await ctx.render('index');
});

export default router;
