import BN from 'bn.js';

/**
* Generate fibonacci sequence
*
* @yields {string}
*/
export default function* fibonacci() {
  let [first, second] = [new BN(0), new BN(1)];

  yield first.toString(10);
  yield second.toString(10);

  while(true) {
    [first, second] = [second, first.add(second)];
    yield second.toString(10);
  }
}
