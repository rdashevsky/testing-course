import React, { Component } from 'react';

import './index.css';

class SequenceTab extends Component {
  render() {
    return (
      <div className={this.props.active ? 'tab active' : 'tab inactive'} >
        <a href={`#/${this.props.tabName}`}
           onClick={(evt) => this.props.active || this.props.onNavigation(evt) }>
          {this.props.tabName}
        </a>
      </div>
    );
  }
}

export default SequenceTab;
